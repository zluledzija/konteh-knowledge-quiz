package com.lzoran.quiz.model;

import javax.persistence.*;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "first_name", length = 32, nullable = false)
	private String firstName;
	@Column(name = "last_name", length = 64, nullable = false)
	private String lastName;
	@Column(name = "email", length = 64, nullable = false)
	private String email;
	@Column(name = "level_of_studies", length = 48)
	private String levelOfStudies;
	@Column(name = "faculty", length = 48)
	private String faculty;
	@Column(name = "department", length = 48)
	private String department;
	@Column(name = "year")
	private Integer year;
	@Column(name = "interested_for_intern")
	private Boolean interestedForIntern;
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id")
	private List<Result> results;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLevelOfStudies() {
		return levelOfStudies;
	}

	public void setLevelOfStudies(String levelOfStudies) {
		this.levelOfStudies = levelOfStudies;
	}

	public String getFaculty() {
		return faculty;
	}

	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Boolean getInterestedForIntern() {
		return interestedForIntern;
	}

	public void setInterestedForIntern(Boolean interestedForIntern) {
		this.interestedForIntern = interestedForIntern;
	}

	public List<Result> getResults() {
		return results;
	}

	public void setResults(List<Result> results) {
		this.results = results;
	}

	public Result getLastResult() {
		Result lastResult = null;
		if (results != null && results.size() > 0) {
			lastResult = Collections.max(results, Comparator.comparing(c -> c.getPoints()));
		}

		return lastResult;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof User) {
			return this.id.equals(((User) obj).getId());
		}

		return false;
	}

	@Override
	public int hashCode() {
		int prime = 31;
		int result = 1;

		return prime * result + (id == null ? 0 : id.hashCode());
	}
}
