package com.lzoran.quiz.model;

import javax.persistence.*;

@Entity
@Table(name = "answer")
public class Answer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "answer_text", length = 64, nullable = false)
	private String answerText;
	@Column(name = "points")
	private Float points;
	@Column(name = "is_correct")
	private Boolean isCorrect;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "question_id")
	private Question question;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAnswerText() {
		return answerText;
	}

	public void setAnswerText(String answerText) {
		this.answerText = answerText;
	}

	public Float getPoints() {
		return points;
	}

	public void setPoints(Float points) {
		this.points = points;
	}

	public Boolean getCorrect() {
		return isCorrect;
	}

	public void setCorrect(Boolean correct) {
		isCorrect = correct;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Question) {
			return this.id.equals(((Question) obj).getId());
		}

		return false;
	}

	@Override
	public int hashCode() {
		int prime = 31;
		int result = 1;

		return prime * result + (id == null ? 0 : id.hashCode());
	}
}
