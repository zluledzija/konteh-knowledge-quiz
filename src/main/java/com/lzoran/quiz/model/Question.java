package com.lzoran.quiz.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "question")
public class Question {

	public static final String SINGLE_ANSWER_QUESTION = "single";
	public static final String MULTIPLE_ANSWERS_QUESTION = "multiple";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "type", length = 16, nullable = false)
	private String type;
	@Column(name = "question_text", length = 64, nullable = false)
	private String questionText;
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	@JoinColumn(name = "question_id")
	private Set<Answer> answers;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getQuestionText() {
		return questionText;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	public Set<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(Set<Answer> answers) {
		this.answers = answers;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Question) {
			return this.id.equals(((Question) obj).getId());
		}

		return false;
	}

	@Override
	public int hashCode() {
		int prime = 31;
		int result = 1;

		return prime * result + (id == null ? 0 : id.hashCode());
	}
}
