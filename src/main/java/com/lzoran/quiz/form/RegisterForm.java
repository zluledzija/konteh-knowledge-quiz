package com.lzoran.quiz.form;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

public class RegisterForm {
	@NotNull
	@Length(min = 1, max = 32)
	private String firstName;
	@NotNull
	@Length(min = 1, max = 64)
	private String lastName;
	@NotNull
	@Length(min = 1, max = 64)
	@Email
	private String email;
	@Length(max = 48)
	private String levelOfStudies;
	@Length(max = 48)
	private String faculty;
	@Length(max = 48)
	private String department;
	private Integer year;
	private Boolean interestedForIntern;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLevelOfStudies() {
		return levelOfStudies;
	}

	public void setLevelOfStudies(String levelOfStudies) {
		this.levelOfStudies = levelOfStudies;
	}

	public String getFaculty() {
		return faculty;
	}

	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Boolean getInterestedForIntern() {
		return interestedForIntern;
	}

	public void setInterestedForIntern(Boolean interestedForIntern) {
		this.interestedForIntern = interestedForIntern;
	}
}
