package com.lzoran.quiz.service;

import com.lzoran.quiz.exception.ServiceException;
import com.lzoran.quiz.model.User;

import java.util.List;

public interface UserService {

	User findUserByEmail(String email) throws ServiceException;

	User save(User user) throws ServiceException;

	List<User> findAll() throws ServiceException;

	User update(User user) throws ServiceException;
}
