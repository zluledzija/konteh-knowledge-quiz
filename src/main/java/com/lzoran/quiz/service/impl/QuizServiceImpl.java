package com.lzoran.quiz.service.impl;

import com.lzoran.quiz.exception.ServiceException;
import com.lzoran.quiz.model.Answer;
import com.lzoran.quiz.model.Question;
import com.lzoran.quiz.repository.QuestionRepository;
import com.lzoran.quiz.service.QuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class QuizServiceImpl implements QuizService {

	@Autowired
	private QuestionRepository questionRepository;

	@Override
	public List<Question> findTenRandomQuestions() {
		return questionRepository.getTenRandomQuestions();
	}

	@Override
	public float evaluateQuizResponses(List<Question> questions, MultiValueMap<String, String> data) {
		float points = 0;

		for (Question question : questions) {
			String questionId = String.valueOf(question.getId());
			if (data.containsKey(questionId)) {
				Integer answerId = Integer.parseInt(data.getFirst(questionId));
				for (Answer answer : question.getAnswers()) {
					if (answer.getId().equals(answerId)) {
						points += answer.getPoints();
						break;
					}
				}
			}
		}

		return points;
	}
}
