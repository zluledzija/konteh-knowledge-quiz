package com.lzoran.quiz.service.impl;

import com.lzoran.quiz.exception.ServiceException;
import com.lzoran.quiz.model.User;
import com.lzoran.quiz.repository.UserRepository;
import com.lzoran.quiz.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public User findUserByEmail(String email) throws ServiceException {
		return userRepository.findByEmail(email);
	}

	@Override
	public User save(User user) throws ServiceException {
		User existingUser = userRepository.findByEmail(user.getEmail());
		if (existingUser != null) {
			throw new ServiceException(String.format("User with email address '%s' already exists.", user.getEmail()));
		}

		return userRepository.save(user);
	}

	@Override
	public List<User> findAll() throws ServiceException {
		return userRepository.findAll();
	}

	@Override
	public User update(User user) throws ServiceException {
		return userRepository.save(user);
	}
}
