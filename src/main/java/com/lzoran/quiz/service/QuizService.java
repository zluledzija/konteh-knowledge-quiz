package com.lzoran.quiz.service;

import com.lzoran.quiz.exception.ServiceException;
import com.lzoran.quiz.model.Question;
import org.springframework.util.MultiValueMap;

import java.util.List;

public interface QuizService {

	List<Question> findTenRandomQuestions() throws ServiceException;

	float evaluateQuizResponses(List<Question> questions, MultiValueMap<String, String> data) throws ServiceException;
}
