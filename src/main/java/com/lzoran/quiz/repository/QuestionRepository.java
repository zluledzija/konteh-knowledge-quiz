package com.lzoran.quiz.repository;

import com.lzoran.quiz.model.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface QuestionRepository extends JpaRepository<Question, Long> {

	@Query(nativeQuery = true, value = "SELECT * FROM question ORDER BY rand() LIMIT 10")
	List<Question> getTenRandomQuestions();
}
