package com.lzoran.quiz.controller;

import com.lzoran.quiz.assembler.UserAssembler;
import com.lzoran.quiz.exception.ServiceException;
import com.lzoran.quiz.form.RegisterForm;
import com.lzoran.quiz.model.User;
import com.lzoran.quiz.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class RegisterController {

	@Autowired
	private UserService userService;
	@Autowired
	private UserAssembler userAssembler;

	private static final Logger log = LoggerFactory.getLogger(RegisterController.class);

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String render(Model model) {
		model.addAttribute("registerForm", new RegisterForm());

		return "register";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String submit(@Valid RegisterForm registerForm, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
		if (bindingResult.hasErrors()) {
			return "register";
		}

		try {
			User user = userService.save(userAssembler.convertToUser(registerForm));
			redirectAttributes.addFlashAttribute("user", user);
		} catch (ServiceException e) {
			log.error(e.getMessage());

			return "register";
		}

		return "redirect:/knowledge-quiz";
	}
}
