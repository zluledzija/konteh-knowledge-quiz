package com.lzoran.quiz.controller;

import com.lzoran.quiz.exception.ServiceException;
import com.lzoran.quiz.model.Question;
import com.lzoran.quiz.model.Result;
import com.lzoran.quiz.model.User;
import com.lzoran.quiz.service.QuizService;
import com.lzoran.quiz.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.List;

@Controller
public class KnowledgeQuizController {

	@Autowired
	private QuizService quizService;
	@Autowired
	private UserService userService;

	private static final Logger log = LoggerFactory.getLogger(KnowledgeQuizController.class);

	@RequestMapping(value = "/knowledge-quiz", method = RequestMethod.GET)
	public String index(Model model, HttpSession session) {
		if (!model.containsAttribute("user")) {
			return "redirect:register";
		}

		try {
			List<Question> questions = quizService.findTenRandomQuestions();
			session.setAttribute("questions", questions);
			session.setAttribute("user", model.asMap().get("user"));
			model.addAttribute("questions", questions);
		} catch (ServiceException e) {
			log.error(e.getMessage());
		}

		return "knowledge-quiz";
	}

	@RequestMapping(value = "/knowledge-quiz", method = RequestMethod.POST)
	public String submit(HttpSession session, @RequestBody(required = false) MultiValueMap<String, String> data) {
		try {
			float points = quizService.evaluateQuizResponses((List<Question>)session.getAttribute("questions"), data);
			Result result = new Result();
			result.setPoints(points);

			User user = (User) session.getAttribute("user");
			user.setResults(Collections.singletonList(result));
			userService.update(user);
		} catch (ServiceException e) {
			log.error(e.getMessage(), e);
		}

		return "redirect:index";
	}
}
