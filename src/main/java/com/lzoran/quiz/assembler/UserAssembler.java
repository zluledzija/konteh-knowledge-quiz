package com.lzoran.quiz.assembler;

import com.lzoran.quiz.form.RegisterForm;
import com.lzoran.quiz.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserAssembler {

	public User convertToUser(RegisterForm registerForm) {
		User user = new User();
		user.setFirstName(registerForm.getFirstName());
		user.setLastName(registerForm.getLastName());
		user.setEmail(registerForm.getEmail());
		user.setLevelOfStudies(registerForm.getLevelOfStudies());
		user.setFaculty(registerForm.getFaculty());
		user.setDepartment(registerForm.getDepartment());
		user.setYear(registerForm.getYear());
		user.setInterestedForIntern(registerForm.getInterestedForIntern());

		return user;
	}
}
