<#ftl>
<#import "/spring.ftl" as spring>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Title of the document</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Konteh Knowledge Quiz</a>
            </div>
            <ul class="nav navbar-nav">
                <li><a href="/index">Home</a></li>
                <li class="active"><a href="/knowledge-quiz">Knowledge Quiz</a></li>
            </ul>
        </div>
    </nav>

    <div>
        <div id="accordion" role="tablist">
            <form method="post">
                <#list questions as question>
                    <div>
                        <div>
                            <h3>${question?counter}. ${question.questionText!}</h3>
                        </div>

                        <div>
                            <#if question.type=='radio'>
                                 <#list question.answers as answer>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="${question.id!}" value="${answer.id!}">
                                            ${answer.answerText!}
                                        </label>
                                    </div>
                                 </#list>
                            <#else>
                                <#list question.answers as answer>
                                   <div class="form-check">
                                       <label class="form-check-label">
                                           <input class="form-check-input" type="checkbox" name="${question.id!}" value="${answer.id}">
                                           ${answer.answerText!}
                                       </label>
                                   </div>
                                </#list>
                            </#if>
                        </div>
                    </div>

                    <hr/>
                </#list>

                <div class="form-group row">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            <form>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>