<#ftl>
<#import "/spring.ftl" as spring>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Title of the document</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Konteh Knowledge Quiz</a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="/index">Home</a></li>
                    <li class="active"><a href="/knowledge-quiz">Knowledge Quiz</a></li>
                </ul>
            </div>
        </nav>

        <div>
            <form method="post">
                <@spring.bind "registerForm" />
                <div class="form-group row">
                    <label for="firstName" class="col-sm-2 col-form-label">First Name</label>
                    <div class="col-sm-5">
                        <@spring.formInput 'registerForm.firstName', 'class="form-control" placeholder="First name"' />
                    </div>
                    <div class="col-sm-5 invalid-feedback">
                           <@spring.showErrors '<br>'/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="lastName" class="col-sm-2 col-form-label">Last Name</label>
                    <div class="col-sm-5">
                        <@spring.formInput 'registerForm.lastName', 'class="form-control" placeholder="Last name"' />
                    </div>
                    <div class="col-sm-5 invalid-feedback">
                           <@spring.showErrors '<br>'/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-5">
                        <@spring.formInput 'registerForm.email', 'class="form-control" placeholder="Email"' />
                    </div>
                    <div class="col-sm-5 invalid-feedback">
                           <@spring.showErrors '<br>'/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="levelOfStudies" class="col-sm-2 col-form-label">Level of studies</label>
                    <div class="col-sm-5">
                        <@spring.formInput 'registerForm.levelOfStudies', 'class="form-control" placeholder="Level of studies"' />
                    </div>
                    <div class="col-sm-5 invalid-feedback">
                           <@spring.showErrors '<br>'/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="faculty" class="col-sm-2 col-form-label">Faculty</label>
                    <div class="col-sm-5">
                        <@spring.formInput 'registerForm.faculty', 'class="form-control" placeholder="Faculty"' />
                    </div>
                    <div class="col-sm-5 invalid-feedback">
                           <@spring.showErrors '<br>'/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="department" class="col-sm-2 col-form-label">Department</label>
                    <div class="col-sm-5">
                        <@spring.formInput 'registerForm.department', 'class="form-control" placeholder="Department"' />
                    </div>
                    <div class="col-sm-5 invalid-feedback">
                           <@spring.showErrors '<br>'/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="year" class="col-sm-2 col-form-label">Year</label>
                    <div class="col-sm-5">
                        <@spring.formInput 'registerForm.year', 'class="form-control" placeholder="Year"', 'number' />
                    </div>
                    <div class="col-sm-5 invalid-feedback">
                           <@spring.showErrors '<br>'/>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <label class="form-check-label">
                                <@spring.formCheckbox 'registerForm.interestedForIntern', 'class="form-check-input"' /> Interested for intern
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Sign in</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>