<#ftl>
<#import "/spring.ftl" as spring>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Title of the document</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Konteh Knowledge Quiz</a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="/index">Home</a></li>
                    <li><a href="/knowledge-quiz">Knowledge Quiz</a></li>
                </ul>
            </div>
        </nav>

        <div>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Level of studies</th>
                    <th scope="col">Faculty</th>
                    <th scope="col">Department</th>
                    <th scope="col">Year</th>
                    <th scope="col">Points</th>
                    <th scope="col">Participated</th>
                </tr>
                </thead>
                <tbody>
                <#list users as user>
                     <tr>
                         <th scope="row">${user?counter}</th>
                         <td>${user.firstName!}</td>
                         <td>${user.lastName!}</td>
                         <td>${user.levelOfStudies!}</td>
                         <td>${user.faculty!}</td>
                         <td>${user.department!}</td>
                         <td>${user.year!}</td>
                         <#if user.getLastResult()?has_content><#assign result = user.getLastResult()></#if>
                         <td><#if result?has_content> ${result.points!}</#if></td>
                         <td><#if result?has_content> ${result.created!}</#if></td>
                     </tr>
                </#list>
                </tbody>
            </table>
        </div>
    </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>